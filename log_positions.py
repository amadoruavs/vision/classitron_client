import time
import mavsdk
import asyncio
import datetime
from mavsdk import System

output = open("urmom.csv", "w")

position = (0, 0, 0)
rotation = (0, 0, 0)

timer = time.time()

async def run():
    # Init the drone
    print("run your mom")
    drone = System()
    await drone.connect(system_address="udp://:14550")

    # Start the tasks
    asyncio.ensure_future(print_position(drone))
    asyncio.ensure_future(print_rotation(drone))

async def print_rotation(drone):
    global rotation
    async for rot in drone.telemetry.attitude_euler():
        # print(rot)
        rotation = (rot.roll_deg, rot.pitch_deg, rot.yaw_deg)

async def print_position(drone):
    global rotation
    global timer
    async for position in drone.telemetry.position():
        if time.time() - timer > 1:
            timer = time.time()
            print(f"{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}|{position.latitude_deg}|{position.longitude_deg}|{position.relative_altitude_m}|{rotation[0]}|{rotation[1]}|{rotation[2]}\n")
            output.write(f"{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}|{position.latitude_deg}|{position.longitude_deg}|{position.relative_altitude_m}|{rotation[0]}|{rotation[1]}|{rotation[2]}\n")


if __name__ == "__main__":

    # Runs the event loop until the program is canceled with e.g. CTRL-C
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    # Start the main function
    loop.create_task(run())
    loop.run_forever()
