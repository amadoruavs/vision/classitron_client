from classitron_client.extensions import celery_app 

@celery_app.task
def classify_task(odlc_id):
    odlc = ODLC.objects.get(odlc_id)
    odlc.alphanumeric = 'W'
    odlc.x = 12
    odlc.y = 69*264*2
    odlc.save()
