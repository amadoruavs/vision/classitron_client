class LoginFailedError(Exception):
    pass

class IMSError(Exception):
    def __init__(self, code, text, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.code = code
        self.text = text

    def __str__(self):
        return f"IMSError({self.code}, {self.text})"
