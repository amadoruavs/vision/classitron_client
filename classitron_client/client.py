from classitron_client.errors import LoginFailedError, IMSError
from classitron_client.types import ODLC, Image, Mission
import requests
import io
from json import JSONDecodeError

class Client(requests.Session):
    def __init__(self, ims_base_url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_url = ims_base_url
        self.token = None

    def get(self, url, *args, **kwargs):
        if self.token is None:
            print("Not logged in! Not making request.")
            return

        headers = { "Authorization": f"Token {self.token}" }
        headers.update(kwargs.get("headers") or {})
        return super().get(url, *args, headers=headers, **kwargs)

    def post(self, url, *args, **kwargs):
        if self.token is None:
            print("Not logged in! Not making request.")
            return

        headers = { "Authorization": f"Token {self.token}" }
        headers.update(kwargs.get("headers") or {})
        return super().post(url, *args, headers=headers, **kwargs)

    def patch(self, url, *args, **kwargs):
        if self.token is None:
            print("Not logged in! Not making request.")
            return

        headers = { "Authorization": f"Token {self.token}" }
        headers.update(kwargs.get("headers") or {})
        return super().patch(url, *args, headers=headers, **kwargs)

    def delete(self, url, *args, **kwargs):
        if self.token is None:
            print("Not logged in! Not making request.")
            return

        headers = { "Authorization": f"Token {self.token}" }
        headers.update(kwargs.get("headers") or {})
        return super().delete(url, *args, headers=headers, **kwargs)

    def get_json(self, url, serialize_class, *args, **kwargs):
        r = self.get(url, *args, **kwargs)
        if r.status_code != 200:
            raise IMSError(r.status_code, r.text)
        else:
            data = r.json()
            if isinstance(data, list):
                sdata = [serialize_class.from_dict(i) for i in data]
            else:
                sdata = serialize_class.from_dict(data)
            return sdata

    def post_json(self, url, serialize_class, *args, **kwargs):
        r = self.post(url, *args, **kwargs)
        if r.status_code not in {200, 201, 204}:
            raise IMSError(r.status_code, r.text)
        else:
            data = r.json()
            print(data)
            sdata = serialize_class.from_dict(data)
            return sdata

    def patch_json(self, url, serialize_class, *args, **kwargs):
        r = self.patch(url, *args, **kwargs)
        if r.status_code not in {200, 201, 204}:
            raise IMSError(r.status_code, r.text)
        else:
            data = r.json()
            print(data)
            sdata = serialize_class.from_dict(data)
            return sdata

    def delete_json(self, url, *args, **kwargs):
        r = self.delete(url, *args, **kwargs)
        if r.status_code != 200:
            raise IMSError(r.status_code, r.text)
        else:
            data = r.json()
            return data

    def login(self, user, password):
        r = super().post(f"{self.base_url}/api/v2/auth/login/",
                      json={
                          "username": user,
                          "password": password,
                      })
        if r.status_code != 200:
            raise LoginFailedError(
                f"Login failed ({r.status_code}), server says: {r.text}")

        try:
            self.token = r.json()["key"]
        except KeyError:
            raise LoginFailedError(
                "Server response did not contain a 'key' attribute.")
        except JSONDecodeError:
            raise LoginFailedError(
                "Server response was in an unexpected (non-JSON) format.")
        return r

    def logout(self):
        return self.post(f"{self.base_url}/api/v2/auth/logout/")

    # --- ODLCs

    def get_odlcs(self):
        return self.get_json(f"{self.base_url}/api/v2/odlcs/", ODLC)

    def get_odlc(self, id: int):
        return self.get_json(f"{self.base_url}/api/v2/odlcs/{id}/", ODLC)

    def post_odlc(self, odlc: ODLC):
        return self.post_json(f"{self.base_url}/api/v2/odlcs/",
                              ODLC, data=odlc.to_dict())

    def update_odlc(self, id: int, odlc: ODLC):
        return self.patch_json(f"{self.base_url}/api/v2/odlcs/{id}/",
                               ODLC, data=odlc.to_dict())

    def upload_odlc_image(self, id: int, image: io.BytesIO):
        return self.patch(f"{self.base_url}/api/v2/odlcs/{id}/",
                          files={"image": image})

    def delete_odlc(self, id: int):
        return self.delete_json(f"{self.base_url}/api/v2/odlcs/{id}/")

    # --- Images

    def get_images(self):
        return self.get_json(f"{self.base_url}/api/v2/images/", Image)

    def get_image_data(self, id: int):
        return self.get_json(f"{self.base_url}/api/v2/images/{id}/", Image)

    def post_image_data(self, image_data: Image):
        print(f"{self.base_url}/api/v2/images/", image_data)
        return self.post_json(f"{self.base_url}/api/v2/images/",
                              Image, data=image_data.to_dict())

    def update_image_data(self, id: int, image_data: Image):
        return self.patch_json(f"{self.base_url}/api/v2/images/{id}/",
                               Image, data=image_data.to_dict())

    def upload_image(self, id: int, image: io.BytesIO):
        return self.patch(f"{self.base_url}/api/v2/images/{id}/",
                          files={"image": image})

    def delete_image(self, id: int):
        return self.delete_json(f"{self.base_url}/api/v2/images/{id}/")

    # --- Missions

    def get_missions(self):
        return self.get_json(f"{self.base_url}/api/v2/missions/", Mission)

    def get_mission(self, id: int):
        return self.get_json(f"{self.base_url}/api/v2/missions/{id}", Mission)

    def post_mission(self, mission: Mission):
        return self.post_json(f"{self.base_url}/api/v2/missions/",
                              Mission, data=mission.to_dict())

    def update_mission(self, id: int, mission: Mission):
        return self.patch_json(f"{self.base_url}/api/v2/missions/{id}/",
                               Mission, data=mission.to_dict())

    def delete_mision(self, id):
        return self.delete_json(f"{self.base_url}/api/v2/missions/{id}/")
