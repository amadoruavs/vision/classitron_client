# classitron_wrapper: a wrapper for uploading to Classitron #

This is a simple Python wrapper to send a POST request to the Classitron classifier platform.

## Architecture ##
Classitron has the following endpoints:
- `/classify` - a list of images to classify; User oriented, do not use w/ scripts.
- `/classify/<int: image_id>/submit/` - POST annotation data here. INTERNAL USE ONLY.
- `/classify/upload` - upload a new Image to the platform to be annotated

This last endpoint is what the wrapper is focused upon. While Classitron will expand and be organized
to be more maintainable and API accessible, it is currently focused towards a working classification app.
Part of this is being able to upload images.

## Usage ##

To upload an image, with metadata supplied:
```python
import classitron_wrapper
lat = 93.222 # Latitude
long = 92.422 # Longitude

ct_client = classitron_wrapper.Client("http://localhost:8000")
ct_client.upload("image_name.jpg", lat, long)
```
