"""
Classitron datatypes
Definitely not copied from interoperator/odlc.py
"""
from typing import List
from dataclasses import dataclass
from enum import Enum

class Color(Enum):
    WHITE = "WHITE"
    BLACK = "BLACK"
    GRAY = "GRAY"
    RED = "RED"
    BLUE = "BLUE"
    GREEN = "GREEN"
    YELLOW = "YELLOW"
    PURPLE = "PURPLE"
    BROWN = "BROWN"
    ORANGE = "ORANGE"


class Shape(Enum):
    CIRCLE = "CIRCLE"
    SEMICIRCLE = "SEMICIRCLE"
    QUARTER_CIRCLE = "QUARTER_CIRCLE"
    TRIANGLE = "TRIANGLE"
    SQUARE = "SQUARE"
    RECTANGLE = "RECTANGLE"
    TRAPEZOID = "TRAPEZOID"
    PENTAGON = "PENTAGON"
    HEXAGON = "HEXAGON"
    HEPTAGON = "HEPTAGON"
    OCTAGON = "OCTAGON"
    STAR = "STAR"
    CROSS = "CROSS"


class Orientation(Enum):
    NORTH = "N"
    NORTHEAST = "NE"
    EAST = "E"
    SOUTHEAST = "SE"
    SOUTH = "S"
    SOUTHWEST = "SW"
    WEST = "W"
    NORTHWEST = "NW"

@dataclass
class Mission:
    mission_id: int
    description: str = ""
    id: int = -1
    url: str = ""

    @staticmethod
    def from_dict(data):
        if "id" in data:
            id = data["id"]
        elif "url" in data:
            id = int(data["url"].split("/")[-2])
        else:
            id = -1

        return Mission(
                id = id,
                url = data["url"],
                mission_id = data["mission_id"],
                description = data["description"],
                )

    def to_dict(self):
        data = {"mission_id": self.mission_id,
                "description": self.description
                }
        if self.id != -1:
            data["id"] = self.id

        return data

@dataclass
class Image:
    mission: str
    latitude: float
    longitude: float
    height: float
    image_width: int
    image_height: int
    url: str = ""
    id: int = -1

    @staticmethod
    def from_dict(data):
        if "id" in data:
            id = data["id"]
        elif "url" in data:
            id = int(data["url"].split("/")[-2])
        else:
            id = -1

        return Image(
                id = id,
                url = data["url"],
                mission = data["mission"],
                latitude = data["latitude"],
                longitude = data["longitude"],
                height = data["height"],
                image_width = data.get("image_width") or -1,
                image_height = data.get("image_height") or -1,
                )

    def to_dict(self):
        data = {"mission": self.mission,
                "latitude": self.latitude,
                "longitude": self.longitude,
                "height": self.height,
                "image_width": self.image_width,
                "image_height": self.image_height,
                }
        if self.id != -1:
            data["id"] = self.id

        return data

@dataclass
class ODLC:
    parent_image: str
    autonomous: bool
    duplicates: List[str]
    x: int
    y: int
    width: int
    height: int

    @staticmethod
    def from_dict(data):
        if "id" in data:
            id = data["id"]
        elif "url" in data:
            id = int(data["url"].split("/")[-2])
        else:
            id = -1

        if not data["is_emergent"]:
            return ODLCStandard.from_dict(data)
        else:
            return ODLCEmergent.from_dict(data)

    def to_dict(self):
        if isinstance(self, ODLCStandard):
            return ODLCStandard.to_dict(self)
        else:
            return ODLCEmergent.to_dict(self)


@dataclass
class ODLCStandard(ODLC):
    orientation: Orientation
    shape: Shape
    shape_color: Color
    alphanumeric: str
    alphanumeric_color: Color
    url: str = ""
    id: int = -1

    @staticmethod
    def from_dict(data):
        if "id" in data:
            id = data["id"]
        else:
            id = -1

        return ODLCStandard(
                id = id,
                url = data["url"],
                parent_image = data["parent_image"],
                orientation = Orientation(data["orientation"]),
                shape = Shape(data["shape"]),
                shape_color = Color(data["shape_color"]),
                autonomous = data["autonomous"],
                alphanumeric = data["alphanumeric"],
                alphanumeric_color = Color(data["alphanumeric_color"]),
                x = data["x"],
                y = data["y"],
                width = data["width"],
                height = data["height"],
                duplicates = data.get("duplicates") or []
                )

    def to_dict(self):
        data = {
            "id": self.id,
            "url": self.url,
            "parent_image": self.parent_image,
            "type": "STANDARD",
            "orientation": self.orientation.value,
            "shape": self.shape.value,
            "shape_color": self.shape_color.value,
            "alphanumeric": self.alphanumeric,
            "alphanumeric_color": self.alphanumeric_color.value,
            "autonomous": self.autonomous,
            "x": self.x,
            "y": self.y,
            "width": self.width,
            "height": self.height,
            "duplicates": self.duplicates
        }
        if self.id != -1:
            data["id"] = self.id

        return data


@dataclass
class ODLCEmergent(ODLC):
    description: str
    url: str = ""
    id: int = -1

    @staticmethod
    def from_dict(data):
        if "id" in data:
            id = data["id"]
        else:
            id = -1

        return ODLCEmergent(
            id = id,
            url = data["url"],
            parent_image = data["parent_image"],
            mission = data["mission"],
            autonomous = data["autonomous"],
            description = data["description"],
            x = data["x"],
            y = data["y"],
            width = data["width"],
            height = data["height"],
        )

    def to_dict(self):
        data = {
            "id": self.id,
            "url": self.url,
            "parent_image": self.parent_image,
            "mission": self.mission,
            "type": "EMERGENT",
            "description": self.description,
            "x": self.w,
            "y": self.y,
            "width": self.width,
            "height": self.height,
        }

        if self.id != -1:
            data["id"] = self.id

        return data

