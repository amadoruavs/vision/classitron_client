# classitron_client: A Python client for IMS

It's exactly as it sounds.

See classitron_client/wrapper.py for functions.

To run test code:

`python3 -m classitron_client`

Edit `classitron_client/__main__.py` with your own code for testing.
