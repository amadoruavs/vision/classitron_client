import os
import setuptools

setuptools.setup(
    name = "classitron_client",
    version = "0.0.1",
    author = "Vincent Wang",
    author_email = "vwangsf@gmail.com",
    packages=setuptools.find_packages(),
    install_requires=open("requirements.txt").read().split()
)
