from math import *
import glob
import cv2
import bisect
from classitron_client.client import Client
from classitron_client.types import ODLCStandard, Image, Mission
from classitron_client.types import Orientation, Shape, Color

DIRECTORY = "/mnt/DCIM/Camera01"
SERVER = "http://172.18.0.19:8080"

client = Client(f"{SERVER}")
client.login("suasdev", "AVuav2018")

latitude = 0
longitude = 0
altitude = 0

logs_f = open("positions.csv")
logs = []
for line in logs_f:
    row = line.strip().split("|")
    logs.append(row[0], float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5]), float(row[6]),)

for img in glob.glob(f"{DIRECTORY}/*.jpg"):
    date = "_".join(img.split("_")[1:3])
    row = bisect.bisect_left(date, logs, key=lambda x: x[0])
    latitude, longitude, altitude = date[row][1:4]
    roll, pitch, yaw = date[row][4:7]
    image_raw = cv2.imread(img)
    offset_x = altitude * tan(radians(roll))
    offset_y = altitude * tan(radians(pitch))

    offset_lon = offset_x / (111111 * math.cos(lat))
    offset_lat = offset_y / 111111

    image_data = client.post_image_data(Image(f"{SERVER}/api/v2/missions/1/", latitude + offset_lat, longitude + offset_lon, altitude, image_raw.shape[1], image_raw.shape[0]))
    print(client.upload_image(image_data.id, open(img, "rb")).text)
